use macroquad::prelude::*;

#[macroquad::main("conflicts")]
async fn main() {
    loop {
        clear_background(WHITE);
        next_frame().await
    }
}
